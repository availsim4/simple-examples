# SPDX-License-Identifier: GPL-3.0-only
# (C) Copyright CERN 2021. All rights not expressly granted are reserved. 

from typing import List

from availsim4core.src.context.context import Context
from availsim4core.src.context.system.component_tree.component import Component
from availsim4core.src.context.system.component_tree.status import Status


class OWNCHILDRENLOGIC:
    """
    Class which defines the Logic between a Component and its Children.
    """

    def __init__(self, dummy_arg = None):
        self.dummy_arg = dummy_arg

    def __repr__(self):
        return f"Not defined"

    def evaluate(self, list_of_children: List[Component], context: Context) -> Status:
        """
        Given a list of Children component, this method evaluates the Status.
        :param list_of_children: The list of Children of the node to evaluate.
        :param context: the context of the simulation (structure containing almost everything)
        :return: The Status of the evaluated node.
        """
        # print(f"list of children is {list_of_children}")
        count = 0
        blinds = 0
        for child in list_of_children:
            # print(f"child's status is {child.status}")
            if child.status != Status.RUNNING :
                count += 1
            if child.status == Status.BLIND_FAILED :
                blinds += 1
        # print(f"count is {count}")
        if count == 0:  return Status.RUNNING
        if count == 1:  return Status.DEGRADED
        if blinds >= 1: return Status.BLIND_FAILED
        return Status.FAILED

