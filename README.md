A series of examples for getting to know the features of AvailSim4. Each example contains an input folder with 
- the necessary input files to run the AvailSim4 model
- a readme describing the model (or the added feature in comparison to the previous model)
and an output folder with example outputs to compare one's results.
